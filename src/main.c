/*----------------------------------------------------------------------------
 *----------------------------------------------------------------------------*/
#include <MKL25Z4.H>
__asm void initialization()
{
	// initialization of outputs
		// type of coffee
			LDR  r4, =0x1FFFFF10  
			MOVS	r0, #0
			STRB  r0, [r4]
			ADDS	r4, #1
			STRB	r0, [r4]
			ADDS	r4, #1
			STRB	r0, [r4]
			ADDS	r4, #1
			STRB	r0, [r4]		

	// sugar
			LDR  r4, =0x1FFFFF20
			STRB  r0, [r4]
			ADDS	r4, #1
			STRB	r0, [r4]
			ADDS	r4, #1
			STRB	r0, [r4]
	// mixer on
			LDR  r4, =0x1FFFFF30
			STRB  r0, [r4]
	// milk		
			LDR  r4, =0x1FFFFF40
			STRB  r0, [r4]

			BX    lr				
}

__asm void cafe()
{
insert_choices
	// insert choices
			LDR  r4, =0x1FFFFF00
			// choose type of coffee
			MOVS	r0, #1 				// 0->gallikos, 1->frapes, 2->nescafe, 3->espresso
			STRB  r0, [r4]
			//choose quantity of sugar
			ADDS	r4, #1
			MOVS	r0, #2    		// 0->sketos, 1->metrios, 2->glykos
			STRB	r0, [r4]
			//choose if you want milk
			ADDS	r4, #1
			MOVS	r0, #1		// 0->no milk, 1->with milk
			STRB	r0, [r4]
	
			//start process
			ADDS	r4, #1		
			MOVS	r0, #0			//0->wait, 1-> start
			STRB	r0, [r4]			
			// shut down
			ADDS	r4, #1		
			MOVS	r0, #0				// 1-> shut down
			STRB	r0, [r4]		
	
	// check if the machine is enabled
			LDR  r4, =0x1FFFFF04
			LDRB  r0, [r4]
			CMP  r0, #1
			BEQ  ending		
			
	// check if the choices are valid		
			SUBS r4, #1
			LDRB r0, [r4]
			CMP r0, #0
			BEQ insert_choices
			
			
			LDR  r4, =0x1FFFFF00
			MOVS r1, #1
			
			LDRB r0, [r4]
			CMP r0, #0
			BEQ gallikos
			CMP r0, #1
			BEQ frapes
			CMP r0, #2
			BEQ nescafe
			CMP r0, #3
			BEQ espresso
			
gallikos
			LDR  r5, =0x1FFFFF10		// turn on the switch for gallikos
			STRB r1, [r5]
			BAL coffee
			
frapes
			LDR  r5, =0x1FFFFF11		// turn on the switch for frapes
			STRB r1, [r5]
			BAL coffee
	
nescafe
			LDR  r5, =0x1FFFFF12		// turn on the switch for nescafe
			STRB r1, [r5]
			BAL coffee	
			
espresso
			LDR  r5, =0x1FFFFF13		// turn on the switch for espresso
			STRB r1, [r5]

coffee
			MOVS r2, #5
coffee_loop										// wait until this process is finished
			SUBS r2, #1
			CMP r2, #0
			BGT coffee_loop
			
			MOVS r3, #0							// turn off the switch
			STRB r3, [r5]
			
			
			ADDS	r4, #1
			LDRB r0, [r4]
			CMP r0, #0
			BEQ sketos
			CMP r0, #1
			BEQ metrios
			CMP r0, #2
			BEQ glykos
			
sketos
			LDR  r5, =0x1FFFFF20			// turn on the switch for sketos
			STRB r1, [r5]
			BAL sugar
			
metrios
			LDR  r5, =0x1FFFFF21			// turn on the switch for metrios
			STRB r1, [r5]
			BAL sugar
	
glykos
			LDR  r5, =0x1FFFFF22			// turn on the switch for glykos
			STRB r1, [r5]
		
		
sugar
			MOVS r2, #5
sugar_loop											// wait until this process is finished
			SUBS r2, #1
			CMP r2, #0
			BGT sugar_loop
			
			STRB r3, [r5]							// turn off the switch

			LDR r5,=0x1FFFFF30				//turn the mixer on
			STRB r1, [r5]
			MOVS r2, #10
mix															// wait until this process is finished	
			SUBS r2, #1
			CMP r2, #0
			BGT mix
			
			STRB r3, [r5]							//turn the mixer off
			
			ADDS	r4, #1
			LDRB r0, [r4]
			CMP r0, #0
			BEQ ready
			
			LDR r5,=0x1FFFFF40				// turn on the switch for milk
			STRB r1, [r5]
			MOVS r2, #5
milk														// wait until this process is finished	
			SUBS r2, #1
			CMP r2, #0
			BGT milk	
			
			STRB r3, [r5]							// turn off the switch for milk
		
ready
			LDR  r4, =0x1FFFFF03			
			STRB r3, [r4]
			
															// the process is finished
			BAL insert_choices

	
  // shut down 
ending
			BX    lr				
}




int main(void)
{
	initialization();
	cafe();
	while (1)
		;
}
// *******************************ARM University Program Copyright � ARM Ltd 2013*************************************   
